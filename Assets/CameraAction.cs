﻿using UnityEngine;
using System.Collections;

public class CameraAction : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 move = new Vector3(0,0,0);
        if (Input.GetMouseButton(0))
        {
            move = this.transform.forward * Time.deltaTime * 200;
        }
        this.GetComponent<CharacterController>().SimpleMove(move);
	}
}
